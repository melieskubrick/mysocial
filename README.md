# My Social
![ReactNative](https://img.shields.io/badge/react_native-%23007ACC.svg?style=for-the-badge&logo=react&logoColor=white)
![JavaScript](https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E)
![TypeScript](https://img.shields.io/badge/typescript-%23007ACC.svg?style=for-the-badge&logo=typescript&logoColor=white)
![Redux](https://img.shields.io/badge/redux_sagas-6DA55F?style=for-the-badge&logo=redux&logoColor=white)
![Axios](https://img.shields.io/badge/axios-%2307405e.svg?style=for-the-badge&logo=axios&logoColor=white)

# Doc
  - [1. Introdução](#1-introdução)
  - [2. Projetos](#2-projetos)
    - [2.1 Aplicação back-end (API)](#21-aplicação-back-end)
    - [2.2 Aplicação front-end (mobile)](#22-aplicação-front-end-mobile)
  - [3. Layout](#3-layout)
  - [4. Pré-requisitos](#4-pré-requisitos)
    - [4.1 Clonar repositório](#41-clonar-repositório)
    - [4.2 Pacotes](#42-pacotes)
  - [5. Execução](#5-execução)
  
## 1. Introdução[](url)
MySocial é uma rede social onde você pode interagir com sua comunidade sobre diversos assuntos.

## 2. Projetos

* ### 2.1 Aplicação back-end
API utilizando [Node.js](https://nodejs.org/) e [MongoDB](https://www.mongodb.com/) que retorna um JSON com as informações dos usuários para uso pelas aplicações front-end. [Link do repositório do Backend](https://gitlab.com/melieskubrick/mysocial-api)

* ###  2.2 Aplicação front-end mobile
Aplicação para celular utilizando [React Native](https://reactnative.dev/) acessando a API do back-end e apresentando os dados no dispositivo móvel.

## 3. Layout
O layout do projeto pode ser visualizado no link [design do projeto](https://www.figma.com/file/H95mB31PtdN085k03rXNFH/MySocial?type=design&node-id=0%3A1&mode=design&t=tDU7zc7FnUJhzABi-1). É necessário ter conta no [Figma](https://figma.com) para acessá-lo.

## 4. Pré-requisitos

### 4.1 Clonar repositório
> Clonar o repositório [Gitlab]
```sh
   git clone https://gitlab.com/melieskubrick/mysocial
   ```  
### 4.2 Pacotes
> Após clonar o projeto por completo rode os comandos abaixo
   ```sh
yarn
   ```
> Se for iOS
   ```sh
yarn
cd ios && pod install
   ```   

## 5. Execução
Para rodar a aplicação segue os comandos
   > Se deseja testar no iOS
   ```sh
   yarn ios
   ```
   ou
   ```sh
   npm run ios
   ```
   > Se deseja testar no Android
   ```sh
   yarn android
   ```
   ou
   ```sh
   npm run android
   ```

## Libs Utilizadas
Aqui estão os pacotes que foram utilizados para a construção deste app!

* [axios](https://github.com/axios/axios)
<br/>O Axios é um cliente HTTP baseado em Promises para fazer requisições
* [redux](https://github.com/reduxjs/react-redux)
<br/>O Redux é uma biblioteca para armazenamento de estados de aplicações
* [yup](https://github.com/jquense/yup)
<br/>Yup é um construtor de esquema para análise e validação de valor de tempo de execução
* [styled-components](https://styled-components.com/)
<br/>ES6 e CSS para estilizar a aplicação
* [redux-sagas](https://redux-saga.js.org/)
<br/>Um gerenciador de efeitos colaterais Redux intuitivo
* [lodash](https://lodash.com/)
<br/>Uma biblioteca de utilitários JavaScript moderna que oferece modularidade

Made with ♥
