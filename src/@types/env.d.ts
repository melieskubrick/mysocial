declare module "@env" {
  export const API_URL: string;
  export const MULTI_AVATAR_API_KEY: string;
  export const MULTI_AVATAR_API_URL: string;
  export const IMAGE_BB_KEY: string;
  export const IMAGE_BB_URL: string;
  export const ENV: "dev" | "prod";
}
