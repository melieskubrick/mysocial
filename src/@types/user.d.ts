export interface IRegister {
  name: string;
  email: string;
  password: string;
}

export interface ILogin {
  email: string;
  password: string;
}

export interface IOtp {
  otp: string;
  email: string;
}

export interface IUser {
  name: string;
  email: string;
  _id: string;
  otp: string;
  createdAt: string;
  updatedAt: string;
  __v: number;
}