
import { useNavigation, useRoute } from "@react-navigation/native";
import { NativeStackNavigationProp } from "@react-navigation/native-stack";


import * as yup from "yup";

import React, { useLayoutEffect, useState } from "react";
import { ActivityIndicator, Alert, Platform } from "react-native";

import { useDispatch } from "react-redux";

import * as S from "./styles";
import { RootStackParamList } from "../../RootStackParamList";
import { IOtp } from "../../@types/user";
import { Formik } from "formik";
import { Input } from "../../components/Input";
import theme from "../../theme";
import Button from "../../components/Button";
import { doVerify } from "../../services/api";

const Verify: React.FC = () => {
  const [loading, setLoading] = useState<boolean>(false);

  const route = useRoute()

  const { email } = route.params

  type navigationProp = NativeStackNavigationProp<
    RootStackParamList,
    "Verify"
  >;
  const navigation = useNavigation<navigationProp>();


  const fetch = async (params: IOtp) => {
    try {
      setLoading(true);
      const res = await doVerify({ ...params, email });

      // storage.clearAll()
      if (res.status === 200) {

        console.log("res", res.data)

        if (res.data[0] === true) {
          Alert.alert("Conta verificada!", "Sucesso")
          navigation.navigate("Login");
        } else {
          Alert.alert("Codigo invalido!", "Verifique novamente")
        }
      }

      if (res.status === 400) {
        return Alert.alert("Houve um erro", `${res.data.message}`);
      }

    } catch (err) {
      return Alert.alert("Houve um erro", `${err.message}`);
    } finally {
      setLoading(false);
    }
  };

  const validationSchema = yup.object().shape({
    otp: yup
      .string()
      .required("*OTP obrigatório"),
  });

  return (
    <S.Container
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      enabled
      keyboardVerticalOffset={Platform.OS === "ios" ? 64 : 0}
    >
      <Formik
        validationSchema={validationSchema}
        initialValues={{ otp: "" }}
        onSubmit={(values) => {
          fetch(values)
        }
        }
      >
        {({ handleChange, handleBlur, errors, handleSubmit }) => (
          <>
            <S.List keyboardShouldPersistTaps="handled">
              <S.Column>
                <S.Title>Verificar</S.Title>
                <Input
                  name="otp"
                  size="medium"
                  keyboardType="email-address"
                  onChangeText={handleChange("otp")}
                  onBlur={handleBlur("otp")}
                  placeholder="OTP"
                  placeholderTextColor={theme.colors.blue}
                />
                {errors.otp && <S.Error>{errors.otp}</S.Error>}
              </S.Column>
            </S.List>
            <S.ContainerButton>
              {loading ? (
                <ActivityIndicator
                  color={theme.colors.gray_light}
                  size="large"
                />
              ) : (
                <Button
                  title="Verificar"
                  color={theme.colors.blue}
                  titleColor={theme.colors.secondary}
                  onPress={() => handleSubmit()}
                />
              )}
            </S.ContainerButton>
          </>
        )}
      </Formik>
    </S.Container>
  );
};

export default Verify;
