import React from 'react';
import { Text, View } from 'react-native';

import * as S from "./styles"
import Button from '../../components/Button';
import { useNavigation } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { RootStackParamList } from '../../RootStackParamList';
import theme from '../../theme';

const Welcome = () => {
  type navigationProp = NativeStackNavigationProp<
    RootStackParamList,
    "Welcome"
  >;
  const navigation = useNavigation<navigationProp>();

  return (
    <S.Container>
      <S.Column />
      <S.Column>

        <S.Title>Seja bem vindo a MySocial</S.Title>
        <S.Description>
          Simplificamos a maneira como você compartilha momentos e se conecta
        </S.Description>
      </S.Column>
      <S.Column>
        <Button
          onPress={() => navigation.navigate("Register")}
          title="Cadastre-se"
          color={theme.colors.blue}
          titleColor={theme.colors.secondary}
        />
        <Button
          onPress={() => navigation.navigate("Login")}
          title="Fazer login"
          titleColor={theme.colors.blue}
        />
      </S.Column>
    </S.Container>
  );

}

export default Welcome;