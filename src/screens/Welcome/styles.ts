import { getStatusBarHeight } from "react-native-status-bar-height";
import styled from "styled-components/native";

export const Container = styled.View`
  flex: 1;
  background-color: ${({ theme }) => theme.colors.primary};
  justify-content: center;
  padding: 0 24px ${getStatusBarHeight()}px 24px;
`;

export const Title = styled.Text`
  font-size: 36px;
  font-family: "Poppins-Bold";
  color: ${({ theme }) => theme.colors.gray_light};
  line-height: 40px;
`;

export const Description = styled.Text`
  font-size: 16px;
  font-family: "Poppins-Regular";
  color: ${({ theme }) => theme.colors.gray_light};
  margin-bottom: 24px;
`;

export const Column = styled.View``;