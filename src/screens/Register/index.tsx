
import { useNavigation } from "@react-navigation/native";
import { NativeStackNavigationProp } from "@react-navigation/native-stack";

import { Formik } from "formik";

import * as yup from "yup";

import React, { useLayoutEffect, useState } from "react";
import { ActivityIndicator, Alert, Platform } from "react-native";

import { useDispatch } from "react-redux";

import * as S from "./styles";
import { RootStackParamList } from "../../RootStackParamList";
import { Input } from "../../components/Input";
import theme from "../../theme";
import Button from "../../components/Button";
import { IRegister } from "../../@types/user";
import { doRegister } from "../../services/api";
import { saveUser } from "../../store/ducks/user/actions";

const Register: React.FC = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [user, setUser] = useState<IRegister>();

  const dispatch = useDispatch();

  type navigationProp = NativeStackNavigationProp<
    RootStackParamList,
    "Register"
  >;
  const navigation = useNavigation<navigationProp>();


  const fetch = async (params: IRegister) => {
    try {
      setLoading(true);
      const res = await doRegister(params);

      // storage.clearAll()
      if (res.status === 200) {
        // storage.set('@TOKEN', res.data.token)
        // storage.set('@USER_DATA', JSON.stringify(res.data))
        dispatch(saveUser(res.data));

        // navigation.navigate("Home");
      }

      if (res.status === 400) {
        return Alert.alert("Houve um erro", `${res.data.message}`);
      }

      Alert.alert("Conta criada com sucesso!", "Verifique a conta acessando seu e-mail")

      setUser(res.data);

      navigation.navigate("Verify", { email: res.data.user.email })
    } catch (err) {
      return Alert.alert("Houve um erro", `${err.message}`);
    } finally {
      setLoading(false);
    }
  };

  const registerValidationSchema = yup.object().shape({
    name: yup.string().required("*Nome obrigatório"),
    email: yup
      .string()
      .email("Por favor, insira um e-mail válido")
      .required("*E-mail obrigatório"),
    password: yup
      .string()
      .min(8, ({ min }) => `A senha deve conter ${min} caracteres`)
      .required("*Senha obrigatória"),
  });

  return (
    <S.Container
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      enabled
      keyboardVerticalOffset={Platform.OS === "ios" ? 64 : 0}
    >
      <Formik
        validationSchema={registerValidationSchema}
        initialValues={{ name: "", email: "", password: "" }}
        onSubmit={(values) => fetch(values)}
      >
        {({ handleChange, handleBlur, errors, handleSubmit }) => (
          <>
            <S.List keyboardShouldPersistTaps="handled">
              <S.Column>
                <S.Title>Cadastro</S.Title>
                <Input
                  name="name"
                  size="medium"
                  keyboardType="email-address"
                  onChangeText={handleChange("name")}
                  onBlur={handleBlur("name")}
                  placeholder="Nome"
                  placeholderTextColor={theme.colors.blue}
                />
                {errors.name && <S.Error>{errors.name}</S.Error>}
                <Input
                  name="email"
                  size="medium"
                  textContentType="emailAddress"
                  keyboardType="email-address"
                  autoCapitalize="none"
                  autoCorrect={false}
                  autoComplete="email"
                  onChangeText={handleChange("email")}
                  onBlur={handleBlur("email")}
                  placeholder="E-mail"
                  placeholderTextColor={theme.colors.blue}
                />
                {errors.email && <S.Error>{errors.email}</S.Error>}
                <Input
                  name="password"
                  size="medium"
                  onChangeText={handleChange("password")}
                  onBlur={handleBlur("password")}
                  placeholder="Senha"
                  placeholderTextColor={theme.colors.blue}
                  secureTextEntry
                />
                {errors.password && <S.Error>{errors.password}</S.Error>}
              </S.Column>
            </S.List>
            <S.ContainerButton>
              {loading ? (
                <ActivityIndicator
                  color={theme.colors.gray_light}
                  size="large"
                />
              ) : (
                <Button
                  title="Cadastrar"
                  color={theme.colors.blue}
                  titleColor={theme.colors.secondary}
                  onPress={() => handleSubmit()}
                />
              )}
            </S.ContainerButton>
          </>
        )}
      </Formik>
    </S.Container>
  );
};

export default Register;
