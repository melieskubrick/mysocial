import { Platform } from "react-native";
import { getStatusBarHeight } from "react-native-status-bar-height";
import styled from "styled-components/native";

export const Container = styled.KeyboardAvoidingView`
  flex: 1;
  padding-bottom: ${getStatusBarHeight()}px;
  justify-content: space-between;
  background-color: ${({ theme }) => theme.colors.primary};
  padding-horizontal: 24px;
`;

export const Column = styled.View``;

export const List = styled.ScrollView.attrs({
  contentContainerStyle: {
    justifyContent: "center",
    flex: 1,
    paddingBottom: getStatusBarHeight(),
  },
})`
  background-color: ${({ theme }) => theme.colors.primary};
  flex: 1;
`;

export const Title = styled.Text`
  font-size: 36px;
  font-family: "Poppins-Bold";
  color: ${({ theme }) => theme.colors.gray_light};
  line-height: 40px;
  margin-bottom: 24px;
`;

export const Error = styled.Text`
  font-size: 16px;
  font-family: "Poppins_700Bold";
  color: ${({ theme }) => theme.colors.red};
  margin-top: 8px;
`;

export const ContainerButton = styled.View`
  padding: 8px 0 ${Platform.OS === "ios" ? getStatusBarHeight(true) / 1.5 : 8}px
    0;
`;
