
import { useNavigation } from "@react-navigation/native";
import { NativeStackNavigationProp } from "@react-navigation/native-stack";

import { HeaderBackButton } from "@react-navigation/elements";

import * as yup from "yup";

import React, { useLayoutEffect, useState } from "react";
import { ActivityIndicator, Alert, Platform } from "react-native";

import * as S from "./styles";
import { RootStackParamList } from "../../RootStackParamList";
import { ILogin } from "../../@types/user";
import { Formik } from "formik";
import { Input } from "../../components/Input";
import theme from "../../theme";
import Button from "../../components/Button";
import { doLogin } from "../../services/api";

const Login: React.FC = () => {
  const [loading, setLoading] = useState<boolean>(false);

  type navigationProp = NativeStackNavigationProp<
    RootStackParamList,
    "Login"
  >;
  const navigation = useNavigation<navigationProp>();

  const fetch = async (params: ILogin) => {
    try {
      setLoading(true);
      const res = await doLogin(params);

      // storage.clearAll()
      if (res.status === 200) {
        if (res.data.user.active === true) {
          Alert.alert("Seja bem vindo(a)", res.data.user.name)
          navigation.navigate("Home");
        } else {
          Alert.alert("Conta não verificada!", "Verifique sua conta antes de fazer login")
          navigation.navigate("Verify", { email: res.data.user.email })
        }

      }

      if (res.status === 400) {
        return Alert.alert("Houve um erro", `${res.data.message}`);
      }

    } catch (err) {
      return Alert.alert("Houve um erro", `${err.message}`);
    } finally {
      setLoading(false);
    }
  };

  const validationSchema = yup.object().shape({
    email: yup
      .string()
      .email("Por favor, insira um e-mail válido")
      .required("*E-mail obrigatório"),
    password: yup
      .string()
      .required("*Senha obrigatória"),
  });

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () =>
        <HeaderBackButton
          onPress={() => {
            navigation.navigate("Welcome")
          }}
        />
    });
  }, [navigation]);

  return (
    <S.Container
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      enabled
      keyboardVerticalOffset={Platform.OS === "ios" ? 64 : 0}
    >
      <Formik
        validationSchema={validationSchema}
        initialValues={{ email: "", password: "" }}
        onSubmit={(values) => {
          fetch(values)
        }
        }
      >
        {({ handleChange, handleBlur, errors, handleSubmit }) => (
          <>
            <S.List keyboardShouldPersistTaps="handled">
              <S.Column>
                <S.Title>Login</S.Title>
                <Input
                  name="email"
                  size="medium"
                  keyboardType="email-address"
                  onChangeText={handleChange("email")}
                  onBlur={handleBlur("email")}
                  placeholder="E-mail"
                  placeholderTextColor={theme.colors.blue}
                />
                {errors.email && <S.Error>{errors.email}</S.Error>}
                <Input
                  name="password"
                  size="medium"
                  onChangeText={handleChange("password")}
                  onBlur={handleBlur("password")}
                  placeholder="Senha"
                  placeholderTextColor={theme.colors.blue}
                  secureTextEntry
                />
                {errors.password && <S.Error>{errors.password}</S.Error>}
              </S.Column>
            </S.List>
            <S.ContainerButton>
              {loading ? (
                <ActivityIndicator
                  color={theme.colors.gray_light}
                  size="large"
                />
              ) : (
                <Button
                  title="Logar"
                  color={theme.colors.blue}
                  titleColor={theme.colors.secondary}
                  onPress={() => handleSubmit()}
                />
              )}
            </S.ContainerButton>
          </>
        )}
      </Formik>
    </S.Container>
  );
};

export default Login;
