import styled from 'styled-components/native';
import theme from '../../theme';

export const Container = styled.View`
  flex:1;
  background-color: ${theme.colors.primary};
  padding: 24px
`;

export const Title = styled.Text`
  font-size: 20px;
  font-family: "Poppins-Bold";
  color: ${({ theme }) => theme.colors.gray_light};
`;

export const Welcome = styled.Text`
  font-size: 14px;
  font-family: "Poppins-Regular";
  color: ${({ theme }) => theme.colors.gray_light};
`;