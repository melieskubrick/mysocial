import React from 'react';

import * as S from "./styles"
import { useSelector } from 'react-redux';
import { RootState } from '../../store';

const Home: React.FC = () => {
  const { user } = useSelector((state: RootState) => state.user);

  return (
    <S.Container>
      <S.Title>Olá {user.name}</S.Title>
      <S.Welcome>Seja bem vindo(a) {user.name}</S.Welcome>
    </S.Container>
  )
}

export default Home;