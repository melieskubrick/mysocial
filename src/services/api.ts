import { API_URL } from "@env";

import axios, { AxiosResponse } from "axios";

import { ILogin, IOtp, IRegister, IUser } from "../@types/user";

const baseURL: string = API_URL;

export const api = axios.create({
  baseURL: baseURL || "https://mysocial-server-be0b07dcacad.herokuapp.com",
});

api.interceptors.request.use(
  async (config) => {
    try {
      // const token = storage.getString('@TOKEN')
      // console.log("token", token);
      // config.headers.authorization = `Bearer ${token}`;
      return config;
    } catch (err) {
      throw new Error("Não foi possível setar o token");
    }
  },
  (error) => {
    Promise.reject(error);
  }
);

export const doRegister = async (
  params: IRegister
): Promise<AxiosResponse<IRegister | any>> => {
  try {
    const res = await api.post("auth/register", params);
    return res;
  } catch (error: any) {
    if (error.response) {
      console.log("err", error);
      return error.response;
    }
    return error;
  }
};

export const doVerify = async (
  params: IOtp
): Promise<AxiosResponse<any>> => {
  try {
    const res = await api.post("auth/verify", params);
    return res;
  } catch (error: any) {
    if (error.response) {
      console.log("err", error);
      return error.response;
    }
    return error;
  }
};

export const doLogin = async (
  params: ILogin
): Promise<AxiosResponse<IUser | any>> => {
  try {
    const res = await api.post("auth/authenticate", params);
    return res;
  } catch (error: any) {
    if (error.response) {
      console.log("err", error.response.data);
      return error.response;
    }
    return error;
  }
};
