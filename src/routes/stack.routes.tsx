import { createNativeStackNavigator } from "@react-navigation/native-stack";
import React from "react";

/* Screens */
import Welcome from "../screens/Welcome";
import theme from "../theme";
import Register from "../screens/Register";
import Verify from "../screens/Verify";
import Login from "../screens/Login";
import Home from "../screens/Home";

const Stack = createNativeStackNavigator();

const navConfig = {
  headerStyle: {
    backgroundColor: theme.colors.primary,
    borderBottomWidth: 0,
    shadowOpacity: 0,
  },
  headerTintColor: theme.colors.secondary,
  headerTitleStyle: {
    fontFamily: "Poppins_600SemiBold",
  },
  headerLeftContainerStyle: {
    paddingLeft: 24,
  },
  headerRightContainerStyle: {
    paddingRight: 24,
  },
};
const MyStack: React.FC = () => {
  const [loaded, setLoaded] = React.useState<boolean>(true);

  const navConfig = {
    headerStyle: {
      backgroundColor: theme.colors.primary,
      borderBottomWidth: 0,
      shadowOpacity: 0,
    },
    headerTintColor: theme.colors.gray_light,
    headerTitleStyle: {
      fontFamily: "Poppins-SemiBold",
      color: theme.colors.gray_light
    },
    headerLeftContainerStyle: {
      paddingLeft: 24,
    },
    headerRightContainerStyle: {
      paddingRight: 24,
    },
  };

  const renderRoutes = () => (
    <>
      <Stack.Navigator initialRouteName="Welcome">
        <Stack.Screen
          name="Welcome"
          component={Welcome}
          options={{ ...navConfig, title: "Bem vindo(a)" }}
        />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{ ...navConfig, title: "Cadastro" }}
        />
        <Stack.Screen
          name="Verify"
          component={Verify}
          options={{ ...navConfig, title: "Verificar" }}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{ ...navConfig, title: "Login" }}
        />
        <Stack.Screen
          name="Home"
          component={Home}
          options={{ ...navConfig, title: "Home" }}
        />
      </Stack.Navigator>
    </>
  );

  const loader = () => <></>;

  return loaded ? renderRoutes() : loader();
};

export default MyStack;
