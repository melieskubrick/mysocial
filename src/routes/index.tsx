import { NavigationContainer } from "@react-navigation/native";
import React from "react";
import { ThemeProvider } from "styled-components/native";

import theme from "../theme";
import MyStack from "./stack.routes";
import { StatusBar } from "react-native";
import { Provider } from "react-redux";
import { store } from "../store";

function Routes() {

  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <StatusBar backgroundColor={theme.colors.gray_light} />
        <NavigationContainer>
          <MyStack />
        </NavigationContainer>
      </ThemeProvider>
    </Provider>
  );
}

export default Routes;
