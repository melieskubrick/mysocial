export default {
  colors: {
    primary: "#EFF2FF",
    secondary: "#FFFFFF",
    black: "#000000",
    gray_light: "#606075",
    red: "#F94B4B",
    blue: "#4285F4",
  },

  typography: {
    light: "Poppins_300Light",
    regular: "Poppins_400Regular",
    medium: "Poppins_500Medium",
    semi_bold: "Poppins_600SemiBold",
    bold: "Poppins_700Bold",
  },
};
