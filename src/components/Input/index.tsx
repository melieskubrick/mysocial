
import React from "react";

import * as S from "./styles";
import theme from "../../theme";
import { TextInputProps } from "react-native";

type Props = TextInputProps & {
  size?: "small" | "medium" | "large";
  icon?: string;
  iconImage?: string;
  name: string;
};

export function Input({
  name,
  iconImage,
  size = "large",
  ...rest
}: Props) {
  return (
    <S.Container>
      {iconImage && <S.IconImage source={iconImage} />}
      <S.TextInput name={name} size={size} {...rest} />
    </S.Container>
  );
}
