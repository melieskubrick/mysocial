import styled, { css } from "styled-components/native";

type TextInputProps = {
  size?: "small" | "medium" | "large";
};

export const Container = styled.View`
  height: 50px;
  background-color: ${({ theme }) => theme.colors.secondary};
  flex-direction: row;
  align-items: center;
  border-radius: 8px;
  margin-top: 8px;
`;

export const TextInput = styled.TextInput<TextInputProps>`
  max-height: 50px;
  height: 50px;
  color: ${({ theme }) => theme.colors.gray_light};
  font-family: "Poppins-Bold";
  margin-left: 16px;
  ${({ size }) =>
    size === "small" &&
    css`
      width: 60px;
      height: 60px;
      text-align: center;
      padding: 0;
    `};

  ${({ size }) =>
    size === "medium" &&
    css`
      flex: 1;
    `};

  ${({ size }) =>
    size === "large" &&
    css`
      width: 100%;
    `};
`;

export const IconImage = styled.Image`
  height: 20px;
  width: 20px;
`;
