import { IUser } from "../../../@types/user";


export enum userActionTypes {
  saveUser = "@user/SAVE_USER",
  logoutUser = "@user/LOGOUT_USER",
}
export interface IUserState {
  user: IUser;
  token: string;
}
