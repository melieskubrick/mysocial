import { produce } from "immer";

import { IUserState, userActionTypes } from "./types";

const INITIAL_STATE = {} as IUserState;

const userReducer = (
  state = INITIAL_STATE,
  action: { type: string; payload: any }
) => {
  return produce(state, (draft: IUserState) => {
    switch (action.type) {
      case userActionTypes.saveUser: {
        const { user, token } = action.payload;

        draft.user = user;
        draft.token = token;

        break;
      }
      case userActionTypes.logoutUser: {
        draft = INITIAL_STATE;
        break;
      }
      default: {
        break;
      }
    }
  });
};

export { userReducer };
