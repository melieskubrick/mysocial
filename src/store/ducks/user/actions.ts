

import { IUser } from "../../../@types/user";
import { userActionTypes } from "./types";

export function saveUser(payload: { user: IUser; token: string }) {
  return {
    type: userActionTypes.saveUser,
    payload,
  };
}

export function logoutUser() {
  return {
    type: userActionTypes.logoutUser,
    payload: {},
  };
}
