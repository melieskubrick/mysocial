import { createStore, applyMiddleware } from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";

import { rootReducer } from "./ducks/rootReducer";
import rootSaga from "./ducks/rootSaga";
import { IUserState } from "./ducks/user/types";

const sagaMiddleware = createSagaMiddleware();

const middlewares = [sagaMiddleware]; // defaults to localStorage for web

const store = createStore(rootReducer, applyMiddleware(...middlewares));

sagaMiddleware.run(rootSaga);

export type RootState = {
  user: IUserState;
};

export type AppDispatch = typeof store.dispatch;

export { store };
